from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager

#Instantierea obiectelor folosite in aplicatie

app = Flask(__name__)

#Secret key pentru cookies
app.config['SECRET_KEY'] = 'abc'

#Setarea fisierului care va urma sa fie baza  de  date
#SQLAlchemy - tool-ul folosit pentru baza de date
#sqlite - libraria folosita pentru baza de date
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///task2.db'

#Instantierea bazei de date
db = SQLAlchemy(app)

#Instantierea encriptiei
bcrypt = Bcrypt(app)

#Instantierea obiectului din libraria LoginManager - flask
#folosita pentru sistemul de log-in
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'

#Importul pus la sfarsit rezolva linkarea circulara
from bookstore import routes