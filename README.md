# Bookstore

`Bookstore` este o aplicatie ce permite mentinerea unei baze de date de carti unde mai multi utilizatori le pot creea, sterge si modifica.

Aplicatia foloseste `python` ca limbaj de programare, `flask` ca framework pentru server si cateva elemente de `boostrap` pentru UI si `sqlite` ca baza de date accesata prin `sqlalchemy`.

## Rulare

```bash
python3 main.py 
```

## Instalare
Pentru a rula aplicatia, anumite pachete trebuie instalante mai intai. Asigurati-va ca urmatorii pasi sunt indepliniti:

* Ultima versiune de python3 este instalata pe platforma dumneavoastra.
* Puteti instala python folosind brew. Rulati comanda asta in terminal:

```bash 
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

* Rulati comanda asta pentru a instala python3:
```bash
$ brew install python3
```

* Instalati pip3 pentru python.

*b Pentru instalare rulati urmatoare comenzi in terminal:
```bash
curl -O https://bootstrap.pypa.io/get-pip.py
```
```bash
sudo python3 get-pip.py
```

* Acum ca pip3 si python3 sunt instalate pe platforma, celelalte pachete sunt gata sa fie instalate pentru a putea rula aplicatia:

Aplicatia foloseste framework-ul flask, asa ca puteti rula urmatoarea comanda (este posibil ca urmatoarele comezi sa ceara permisiune, asta poate fi rezolvat prin adaugarea optiunii `sudo` in fata lor):

```bash
pip3 install flask
```

Anumite pachete care apartin framework-ului flask trebuie instalate:

1. flask-wtf:
```bash
pip3 install flask-wtf
```
Pachetul asta este pentru a crea form-uri.

2. flask-sqlalchemy:
```bash
pip3 install flask-sqlalchemy
```
Acest pachet este pentru a crea si accesa o baza de date.

3. flask-bcrypt:
```bash
pip3 install flask-bcrypt
```
Pachetul acesta face posibila encriptarea parolei si verificarea encriptiei cu fiecare sesiune de login.

4. flask-login:
```bash
pip3 install flask-login
```
Pachetul acesta face posibila login-ul si permisiunea user-ului: a verifica daca user-ul este deja logat, si a-i prezenta optiunile cand este logat.

## Set-up

Pentru ca aplicatia sa functioneze, baza de date trebuie intializata:
```bash
: python3
>>> from bookstore import db
>>> db.create_all()
```

## Screenshots

<img src="./docs/register.png">
<img src="./docs/login.png ">
<img src="./docs/new.png ">
<img src="./docs/home.png ">
<img src="./docs/edit.png ">